fzf-git-projs() {
    local candidates=()

    zstyle -a :mt:fzf:git-projs dirs _plugin__dirs
    for d in ${_plugin__dirs}; do
        git_projs="$(find ${d} -maxdepth 3 -name .git -exec dirname {} \; | sort)"
        projectile_projs="$(find ${d} -maxdepth 3 -name .projectile -exec dirname {} \; | sort)"
        candidates=(${candidates} ${(f)git_projs} ${(f)projectile_projs})
    done

    local proj=$(for c in ${candidates[@]}; do print ${c}; done \
        | FZF_DEFAULT_OPTS="${FZF_DEFAULT_OPTS}
            --bind=\"tab:accept\"
            --bind=\"enter:become@print 'cd {}'@\"
            " fzf --ansi)

    if [[ -n "${proj}" ]]; then
        if [[ "${proj}" =~ '^cd (.+)$' ]]; then
            zle push-line
            BUFFER="${=proj}"
            zle accept-line
            local ret=$?
            zle reset-prompt
            return ${ret}
        else
            LBUFFER="${LBUFFER} ${proj}"
            zle redisplay
        fi
    fi
}
zle -N fzf-git-projs
