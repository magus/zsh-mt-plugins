# -*- shell-script -*-

# configurable behaviour:
# - :mt:modules:zellij autostart
#     control auto start of zellij in new shells, <boolean>

do_zellij=yes
if [[ -n "${INSIDE_EMACS}" ]]; then
    do_zellij=no
elif [[ "${TERM}" == "linux" ]]; then
    do_zellij=no
elif [[ -z "${commands[zellij]}" ]]; then
    do_zellij=no
fi

if [[ "${do_zellij}" == "yes" ]]; then
    if zstyle -t :mt:modules:zellij autostart; then
        [[ -z "${ZELLIJ}" ]] && exec zellij
    fi
fi
