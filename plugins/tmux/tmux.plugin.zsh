# -*- shell-script -*-

# configurable behaviour:
# - :mt:modules:tmux autostart
#     control auto start of tmux in new shells, <boolean>/'ssh'

alias tmuxa='tmux attach -t'
alias tmuxs='tmux new-session -s'
alias tmuxl='tmux list-sessions'

do_tmux=yes
if [[ -n "${INSIDE_EMACS}" ]]; then
    do_tmux=no
elif [[ "${TERM}" == "linux" ]]; then
    do_tmux=no
elif [[ -z "${commands[tmux]}" ]]; then
    do_tmux=no
fi

if [[ "${do_tmux}" == "yes" ]]; then
    if zstyle -t :mt:modules:tmux autostart; then
        [[ -z "${TMUX}" ]] && exec tmux new-session
    fi
fi
