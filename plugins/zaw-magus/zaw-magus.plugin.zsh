if [[ -n $(declare -f -F zaw-register-src) ]]; then
    here=${${0:A}:h}
    for fn in ${here}/sources/*.zsh; do
        source ${fn}
    done
else
    echo "zaw-magthe is not loaded since zaw is not loaded."
    echo "Please load zaw first."
fi
