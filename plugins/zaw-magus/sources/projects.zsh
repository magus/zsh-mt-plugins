function magus-projects() {
    candidates=()

    zstyle -a :mt:modules:zaw dirs _plugin__dirs
    for d in ${_plugin__dirs}; do
        git_projs="$(find ${d} -maxdepth 3 -name .git -exec dirname {} \; | sort)"
        projectile_projs="$(find ${d} -maxdepth 3 -name .projectile -exec dirname {} \; | sort)"
        candidates=(${candidates} ${(f)git_projs} ${(f)projectile_projs})
    done

    actions=(magus-projects-cd
             magus-projects-edit-first
             magus-projects-tmux
            )
    act_descriptions=("cd/pushd"
                      "edit"
                      "new tmux win"
                     )
}

function magus-projects-cd() {
    if zstyle -t :mt:modules:zaw pushd; then
        zaw-callback-execute "pushd ${1}"
    else
        zaw-callback-execute "cd ${1}"
    fi
}

function magus-projects-edit-first() {
    if zstyle -t :mt:modules:zaw pushd; then
        zaw-callback-replace-buffer "pushd ${1}"
    else
        zaw-callback-replace-buffer "cd ${1}"
    fi
}

function magus-projects-tmux() {
    nd=${1}
    wn=$(basename ${nd})
    zaw-callback-execute "tmux new-window -n ${wn} -c ${nd}"
}

zaw-register-src -n git-projs magus-projects
