# Set the variables necessary for a working gpg-agent
if [[ -S ${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh ]]; then
    export SSH_AUTH_SOCK=${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh
elif [[ -S ${HOME}/.gnupg/S.gpg-agent.ssh ]]; then
   export SSH_AUTH_SOCK=${HOME}/.gnupg/S.gpg-agent.ssh
fi

export GPG_TTY=$(tty)

# add this???? gpg-connect-agent updatestartuptty /bye >/dev/null
